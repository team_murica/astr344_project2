import numpy as np
import random
import matplotlib.pyplot as plt


# setting up the animation

plt.axis([0,100,-2,2])
plt.ion()
plt.show()
  
# define Car object
class Car:
	def __init__(self, Xposition = 0.0, Yposition = 0.0, velocity = 1.0, end = False):
		self.Xposition = Xposition
		self.Yposition = Yposition
		self.velocity = velocity
		self.end = end

	def move(self):
		self.Xposition = self.Xposition + self.velocity


cars = [Car()]
Ntotal = 100 # total number of cars 
P = 0.9 # probability that a deer jumps out every second(?)
N = 20 # N cars behind the car which saw the deer get slowed down 
fv = 0.8 # velocity drop 
L = 100 # length of highway

#for i in range(Ntotal):
#	cars.append(Car(Xposition = -i))

time = 0
waittime = 0

while any(car.end == False for car in cars):

	# put speed back to 1
	for car in cars:
		car.velocity = 1.0

	# generate deer
	deer = random.randint(0,L)
	
	prob = random.random()
	if prob < P: # when a deer jumps out

		# find the position of the car closest to the deer (excluding those which already pass the deer)
		all_cars_going_towards_deer = [car.Xposition for car in cars if car.Xposition <= deer]
		if len(all_cars_going_towards_deer) != 0:
			closest_car = max(all_cars_going_towards_deer)

			# the car gets scared if deer is <= 1 mile away 
			if deer - closest_car <= 1:
				
				# get index 
				ind = [car.Xposition for car in cars].index(closest_car)
				
				# slow down this car + N cars behind it
				for k in range(ind, ind+N+1):
					try:
						cars[k].velocity *= fv
					except IndexError:
						pass

	# make sure that no car will crash into the car in front after next move
	# if any car will crash into the front car, slow down its velocity so that 
	#   it has the same velocity as the front car does
	for i in range(len(cars)-1):
		if cars[i+1].Xposition + cars[i+1].velocity >= cars[i].Xposition + cars[i].velocity:
			cars[i+1].velocity = cars[i].velocity
		assert cars[i].Xposition > cars[i+1].Xposition, "Overtake!"
	
	# check if there's a car at marker 0 and check if it is slowed down 
	# if it is, the next car needs to wait for 5 seconds to enter
	if (cars[-1].Xposition == 0) and (cars[-1].velocity < 1):
		waittime = 5
	
	# move cars
	for car in cars:
		car.move()
		if car.Xposition > L:
			car.end = True

	# append a new car
	if (len(cars) < Ntotal) and (waittime == 0):
		cars.append(Car())
	
	time += 1
	if waittime > 0: waittime -= 1

	# plot
	Xpositions = np.array([car.Xposition for car in cars])
	Ypositions = np.array([car.Yposition for car in cars])
	plt.clf()
	plt.text(5, -0.8, 'Time = '+str(time))
	plt.scatter(Xpositions,Ypositions, s=15, c='r', marker='.')
	if prob < P:
		plt.plot([deer, deer], [-0.1,0.1], 'b-', linewidth= 1)   # plot deer
	plt.xlim(0,L)
	plt.ylim(-1,1)
	plt.draw()
	plt.pause(0.01)


print time

